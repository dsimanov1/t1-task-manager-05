package ru.t1.simanov.tm;

import ru.t1.simanov.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            showErrorArgument();
            return;
        }
        final String arg = args[0];
        if (TerminalConst.VERSION.equals(arg)) {
            showVersion();
            return;
        }
        if (TerminalConst.ABOUT.equals(arg)) {
            showAbout();
            return;
        }
        if (TerminalConst.HELP.equals(arg)) {
            showHelp();
            return;
        }
        showErrorArgument();
    }

    public static void showErrorArgument() {
        System.err.println("Error! This argument not supported...");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Dima Simanov");
        System.out.println("E-mail: simanov.dima@gmail.com");
        System.out.println("E-mail: dsimanov@t1-consulting.ru");
        System.out.println("Site: https://gitlab.com/dsimanov1");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show application version. \n", TerminalConst.VERSION);
        System.out.printf("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application commands. \n", TerminalConst.HELP);
    }

}
