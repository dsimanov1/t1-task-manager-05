package ru.t1.simanov.tm.constant;

public class TerminalConst {

    public final static String VERSION = "version";

    public final static String ABOUT = "about";

    public final static String HELP = "help";

}
